﻿using DataAccess.Contracts;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class EmployeeRepository : MasterRepository, IEmployeeRepository
    {
        private string selectAll;
        private string insert;
        private string update;
        private string delete;

        public EmployeeRepository()
        {
            this.selectAll = "select * from Employee";
            this.insert = "insert into Employee values(@identification, @inss, @firstname, @lastname, @address, @phone, @gender, @salary)";
            this.update = "update table Employee set identification = @identification, inss = @inss, firstname = @firstname, lastname = @lastname, address = @address, phone = @phone, gender = @gender, salary = @salary where id = @id";
            this.delete = "delete from Employee where id = @id";
        }

        public int Add(Employee entity)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@identification", entity.Identification));
            parameters.Add(new SqlParameter("@inss", entity.Inss));
            parameters.Add(new SqlParameter("@firstname", entity.Firstname));
            parameters.Add(new SqlParameter("@lastname", entity.Lastname));
            parameters.Add(new SqlParameter("@address", entity.Address));
            parameters.Add(new SqlParameter("@phone", entity.Phone));
            parameters.Add(new SqlParameter("@gender", entity.Gender));
            parameters.Add(new SqlParameter("@salary", entity.Salary));
            return ExecuteNonQuery(insert);
        }

        public int Delete(int id)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@id", id));
            return ExecuteNonQuery(delete);
        }

        public int Edit(Employee entity)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@id", entity.Id));
            parameters.Add(new SqlParameter("@identification", entity.Identification));
            parameters.Add(new SqlParameter("@inss", entity.Inss));
            parameters.Add(new SqlParameter("@firstname", entity.Firstname));
            parameters.Add(new SqlParameter("@lastname", entity.Lastname));
            parameters.Add(new SqlParameter("@address", entity.Address));
            parameters.Add(new SqlParameter("@phone", entity.Phone));
            parameters.Add(new SqlParameter("@gender", entity.Gender));
            parameters.Add(new SqlParameter("@salary", entity.Salary));
            return ExecuteNonQuery(update);
        }

        public Employee FindById(int id)
        {
            Employee employee = null;
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@id", id));
            var table = ExecuteReader("Select * from Employee where id = @id");
            if(table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                employee = new Employee
                {
                    Id = int.Parse(row[0].ToString()),
                    Identification = row[1].ToString(),
                    Inss = row[2].ToString(),
                    Firstname = row[3].ToString(),
                    Lastname = row[4].ToString(),
                    Address = row[5].ToString(),
                    Phone = row[6].ToString(),
                    Gender = row[7].ToString(),
                    Salary = double.Parse(row[8].ToString()),
                };
            }

            return employee;
        }

        public IEnumerable<Employee> GetAll()
        {
            var employees = new List<Employee>();
            var table = ExecuteReader(selectAll);
            foreach (DataRow row in table.Rows)
            {
                employees.Add(new Employee
                {
                    Id = int.Parse(row[0].ToString()),
                    Identification = row[1].ToString(),
                    Inss = row[2].ToString(),
                    Firstname = row[3].ToString(),
                    Lastname = row[4].ToString(),
                    Address = row[5].ToString(),
                    Phone = row[6].ToString(),
                    Gender = row[7].ToString(),
                    Salary = double.Parse(row[8].ToString()),
                });
            }

            return employees;
        }
    }
}
