﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Product
    {
        private int id;
        private string sku;
        private string name;
        private string description;
        private int stock;
        private double price;

        public int Id { get => id; set => id = value; }
        public string Sku { get => sku; set => sku = value; }
        public string Name { get => name; set => name = value; }
        public string Description { get => description; set => description = value; }
        public int Stock { get => stock; set => stock = value; }
        public double Price { get => price; set => price = value; }
    }
}
