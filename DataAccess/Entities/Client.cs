﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Client
    {
        private int id;
        private string identification;
        private string firstname;
        private string lastname;
        private string phone;
        private string email;
        private string address;

        public int Id { get => id; set => id = value; }
        public string Identification { get => identification; set => identification = value; }
        public string Firstname { get => firstname; set => firstname = value; }
        public string Lastname { get => lastname; set => lastname = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Email { get => email; set => email = value; }
        public string Address { get => address; set => address = value; }
    }
}
