﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Bill
    {
        private int id;
        private string code;
        private DateTime date;
        private Employee employee;
        private Client client;
        private string description;
        private double subtotal;
        private double vat;
        private double total;

        public int Id { get => id; set => id = value; }
        public string Code { get => code; set => code = value; }
        public DateTime Date { get => date; set => date = value; }
        public Employee Employee { get => employee; set => employee = value; }
        public Client Client { get => client; set => client = value; }
        public string Description { get => description; set => description = value; }
        public double Subtotal { get => subtotal; set => subtotal = value; }
        public double Vat { get => vat; set => vat = value; }
        public double Total { get => total; set => total = value; }
    }
}
