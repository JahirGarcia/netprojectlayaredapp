﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Employee
    {
        private int id;
        private string identification;
        private string inss;
        private string firstname;
        private string lastname;
        private string address;
        private string phone;
        private string gender;
        private double salary;

        public int Id { get => id; set => id = value; }
        public string Identification { get => identification; set => identification = value; }
        public string Inss { get => inss; set => inss = value; }
        public string Firstname { get => firstname; set => firstname = value; }
        public string Lastname { get => lastname; set => lastname = value; }
        public string Address { get => address; set => address = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Gender { get => gender; set => gender = value; }
        public double Salary { get => salary; set => salary = value; }
    }
}
