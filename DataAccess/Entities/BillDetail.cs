﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class BillDetail
    {
        private int id;
        private Bill bill;
        private Product product;
        private int quantity;
        private double price;

        public int Id { get => id; set => id = value; }
        public Bill Bill { get => bill; set => bill = value; }
        public Product Product { get => product; set => product = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public double Price { get => price; set => price = value; }
    }
}
