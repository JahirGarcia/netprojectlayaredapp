﻿using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Repositories;
using Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class EmployeeModel
    {
        private int id;
        private string identification;
        private string inss;
        private string firstname;
        private string lastname;
        private string address;
        private string phone;
        private string gender;
        private double salary;

        private IEmployeeRepository empleadoRepository;
        private EntityState state;

        public EmployeeModel()
        {
            empleadoRepository = new EmployeeRepository();
        }

        public int Id { get => id; private set => id = value; }
        public string Identification { get => identification; private set => identification = value; }
        public string Inss { get => inss; set => inss = value; }
        public string Firstname { get => firstname; set => firstname = value; }
        public string Lastname { get => lastname; set => lastname = value; }
        public string Address { get => address; set => address = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Gender { get => gender; set => gender = value; }
        public double Salary { get => salary; set => salary = value; }
        public EntityState State { private get => state; set => state = value; }

        public List<EmployeeModel> GetAll()
        {
            var employeeDataModel = empleadoRepository.GetAll();
            var employeeList = new List<EmployeeModel>();
            foreach(Employee employee in employeeDataModel)
            {
                employeeList.Add(new EmployeeModel
                {
                    id = employee.Id,
                    identification = employee.Identification,
                    inss = employee.Inss,
                    firstname = employee.Firstname,
                    lastname = employee.Lastname,
                    address = employee.Address,
                    phone = employee.Phone,
                    gender = employee.Gender,
                    salary = employee.Salary,
            });
            }

            return employeeList;
        }

        public string SaveChanges(EmployeeModel employee)
        {
            string message = "";
            try
            {
                var employeeDataModel = new Employee();
                employeeDataModel.Id = id;
                employeeDataModel.Identification = identification;
                employeeDataModel.Inss = inss;
                employeeDataModel.Firstname = firstname;
                employeeDataModel.Lastname = lastname;
                employeeDataModel.Address = address;
                employeeDataModel.Phone = phone;
                employeeDataModel.Gender = gender;
                employeeDataModel.Salary = salary;

                switch(State)
                {
                    case EntityState.Added:
                        empleadoRepository.Add(employeeDataModel);
                        message = "Successfully record";
                        break;
                    case EntityState.Modified:
                        empleadoRepository.Edit(employeeDataModel);
                        message = "Successfully updated";
                        break;
                    case EntityState.Deleted:
                        empleadoRepository.Delete(id);
                        message = "Successfully deleted";
                        break;
                }
            }
            catch(Exception ex)
            {
                SqlException sqlEx = ex as SqlException;
                if(sqlEx != null && sqlEx.Number == 2627)
                {
                    message = "Duplicate record";
                } else
                {
                    message = ex.ToString();
                }
            }

            return message;
        }
    }
}
